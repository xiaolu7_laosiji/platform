var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');
Page({
  sendMsg(e) {
    const self = this
    const { formId } = e.detail
    const formData = e.detail.value
    console.log('form_id is:', formId)
    util.request("http://192.168.3.9/api/msg/msg", {formId:formId}).then(function (res) {
      if (res.errno === 0) {

        wx.hideLoading();

        wx.showToast({
          title: res.data,
          icon: 'success',
          duration: 2000,
          complete: function () {
            that.setData({
              index: 0,
              content: '',
              contentLength: 0,
              mobile: ''
            });
          }
        });
      } else {
        util.showErrorToast(res.data);
      }

    });
  }
})