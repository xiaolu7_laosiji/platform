package com.platform.entity;

import java.io.Serializable;

public class WxMsg implements Serializable {
	
	private static final long serialVersionUID = -5416391845105564987L;
	private String value;
	private String color = "#173177";
	public WxMsg() {
	}
	public WxMsg(String value) {
		this.value = value;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
}
