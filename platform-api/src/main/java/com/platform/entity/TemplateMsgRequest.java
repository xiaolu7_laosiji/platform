package com.platform.entity;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

public class TemplateMsgRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4076087441395750203L;
	private String openId;
	/**
	 * https://mp.weixin.qq.com/advanced/tmplmsg?action=list&t=tmplmsg/list&token=529819432&lang=zh_CN
	 */
	private String templateId;
	private String url;
	private Map<String, WxMsg> msg = new LinkedHashMap<>(8); 
	public TemplateMsgRequest(String templateId) {
		this("", templateId);
	}
	public TemplateMsgRequest(String openId, String templateId) {
		super();
		this.openId = openId;
		this.templateId = templateId;
	}
	public String getOpenId() {
		return openId;
	}
	public String getTemplateId() {
		return templateId;
	}
	public String getUrl() {
		return url;
	}
	public TemplateMsgRequest setUrl(String url) {
		this.url = url;
		return this;
	}
	public TemplateMsgRequest setOpenId(String openId) {
		this.openId = openId;
		return this;
	}
	public TemplateMsgRequest setMsg(String key, String msg) {
		this.msg.put(key, new WxMsg(msg));
		return this;
	}
	public Map<String, WxMsg> getMsg() {
		return msg;
	}
	public String getMsg(String key) {
		WxMsg w = this.msg.get(key);
		if (w == null) {
			return "";
		}
		return w.getValue();
	}

}
