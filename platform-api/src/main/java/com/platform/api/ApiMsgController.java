package com.platform.api;

import com.platform.annotation.LoginUser;
import com.platform.entity.TemplateMsgRequest;
import com.platform.entity.UserVo;
import com.platform.service.WeixinService;
import com.platform.util.ApiBaseAction;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/api/msg")
public class ApiMsgController extends ApiBaseAction {

    @Autowired
    private WeixinService weixinService;

    @PostMapping("msg")
    public Object msg(@LoginUser UserVo loginUser, String formId) {
        String openId = loginUser.getWeixin_openid();
        TemplateMsgRequest req = new TemplateMsgRequest("QutRwRkpyCcWidklXUUkSb0UOymWkuVHzf2DhdscScE");
        req.setUrl("");
        req.setMsg("first", "你好，你与客户约定的到场时间即将到达，记得准时到场哦。");
        String regtimeStr = DateFormatUtils.format(new Date(),
                "yyyy-MM-dd HH:mm");
        req.setMsg("keyword1", RandomStringUtils.randomAlphanumeric(16));
        req.setMsg("keyword2", regtimeStr);
        req.setMsg("remark", "");
        req.setOpenId(openId);
        this.weixinService.sendMsg(req, formId);
        return toResponsSuccess("操作成功");
    }
}
